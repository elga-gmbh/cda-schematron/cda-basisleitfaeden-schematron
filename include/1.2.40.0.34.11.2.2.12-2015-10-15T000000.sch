<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.2.2.12
Name: Zusammenfassung des Aufenthalts
Description: Kurzbeschreibung des Verlaufs des stat. Aufenthaltes (Dekurs).
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.2.2.12-2015-10-15T000000">
   <title>Zusammenfassung des Aufenthalts</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]
Item: (Aufenthaltszusammenfassung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]
Item: (Aufenthaltszusammenfassung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]"
         id="d42e4039-false-d54218e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.12']) &gt;= 1">(Aufenthaltszusammenfassung): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.12']) &lt;= 1">(Aufenthaltszusammenfassung): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']) &gt;= 1">(Aufenthaltszusammenfassung): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']) &lt;= 1">(Aufenthaltszusammenfassung): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:code[(@code = '8648-8' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(Aufenthaltszusammenfassung): Element hl7:code[(@code = '8648-8' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:code[(@code = '8648-8' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(Aufenthaltszusammenfassung): Element hl7:code[(@code = '8648-8' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(Aufenthaltszusammenfassung): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(Aufenthaltszusammenfassung): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(Aufenthaltszusammenfassung): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(Aufenthaltszusammenfassung): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.12']
Item: (Aufenthaltszusammenfassung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.12']"
         id="d42e4041-false-d54277e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(Aufenthaltszusammenfassung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.2.2.12')">(Aufenthaltszusammenfassung): Der Wert von root MUSS '1.2.40.0.34.11.2.2.12' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']
Item: (Aufenthaltszusammenfassung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']"
         id="d42e4046-false-d54292e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(Aufenthaltszusammenfassung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.5.3.1.3.5')">(Aufenthaltszusammenfassung): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.5.3.1.3.5' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:code[(@code = '8648-8' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (Aufenthaltszusammenfassung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:code[(@code = '8648-8' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e4051-false-d54307e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(Aufenthaltszusammenfassung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="@nullFlavor or (@code='8648-8' and @codeSystem='2.16.840.1.113883.6.1')">(Aufenthaltszusammenfassung): Der Elementinhalt MUSS einer von 'code '8648-8' codeSystem '2.16.840.1.113883.6.1'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:title[not(@nullFlavor)]
Item: (Aufenthaltszusammenfassung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:title[not(@nullFlavor)]"
         id="d42e4059-false-d54323e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(Aufenthaltszusammenfassung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="text()='Zusammenfassung des Aufenthalts'">(Aufenthaltszusammenfassung): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Zusammenfassung des Aufenthalts'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:text[not(@nullFlavor)]
Item: (Aufenthaltszusammenfassung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:text[not(@nullFlavor)]"
         id="d42e4065-false-d54337e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2.2.12-2015-10-15T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(Aufenthaltszusammenfassung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.12
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.12'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.5']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]
Item: (Aufenthaltszusammenfassung)
--></pattern>
