<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.3.2.18
Name: Pflegerelevante Informationen zur medizinischen Behandlung
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.3.2.18-2015-09-17T000000">
   <title>Pflegerelevante Informationen zur medizinischen Behandlung</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]"
         id="d42e8885-false-d81277e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']) &gt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:templateId[@root = '1.2.40.0.34.11.3.2.18'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']) &lt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:templateId[@root = '1.2.40.0.34.11.3.2.18'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')]) &gt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')]) &lt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]) &lt;= 1">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']"
         id="d42e8887-false-d81335e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.3.2.18')">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Der Wert von root MUSS '1.2.40.0.34.11.3.2.18' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:code[(@code = 'PFMEDBEH' and @codeSystem = '1.2.40.0.34.5.40')]"
         id="d42e8892-false-d81350e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="@nullFlavor or (@code='PFMEDBEH' and @codeSystem='1.2.40.0.34.5.40')">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Der Elementinhalt MUSS einer von 'code 'PFMEDBEH' codeSystem '1.2.40.0.34.5.40'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:title[not(@nullFlavor)]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:title[not(@nullFlavor)]"
         id="d42e8900-false-d81366e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="text()='Pflegerelevante Informationen zur medizinischen Behandlung'">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Pflegerelevante Informationen zur medizinischen Behandlung'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:text[not(@nullFlavor)]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:text[not(@nullFlavor)]"
         id="d42e8906-false-d81380e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.18-2015-09-17T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(Pflegerelevanteinformationenzurmedizinischenbehandlung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.8']]]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.18
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.18']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.1.2.9']]]
Item: (Pflegerelevanteinformationenzurmedizinischenbehandlung)
--></pattern>
