<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.3.2.1
Name: Pflege- und Betreuungsdiagnosen (enhanced)
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.3.2.1-2013-04-17T000000">
   <title>Pflege- und Betreuungsdiagnosen (enhanced)</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]
Item: (PflegeBetreuungsdiagnosenEnhanced)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]
Item: (PflegeBetreuungsdiagnosenEnhanced)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]"
         id="d42e8389-false-d79641e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']) &gt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:templateId[@root = '1.2.40.0.34.11.3.2.1'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']) &lt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:templateId[@root = '1.2.40.0.34.11.3.2.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')]) &gt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')]) &lt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="count(hl7:entry) = 0">(PflegeBetreuungsdiagnosenEnhanced): Element hl7:entry DARF NICHT vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']
Item: (PflegeBetreuungsdiagnosenEnhanced)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']"
         id="d42e8393-false-d79694e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(PflegeBetreuungsdiagnosenEnhanced): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.3.2.1-2013-04-17T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.3.2.1')">(PflegeBetreuungsdiagnosenEnhanced): Der Wert von root MUSS '1.2.40.0.34.11.3.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.30011
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')]
Item: (PflegeBetreuungsdiagnosenAlleEIS)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:code[(@code = 'PFDIAG' and @codeSystem = '1.2.40.0.34.5.40')]"
         id="d79695e55-false-d79710e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.30011-2015-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(PflegeBetreuungsdiagnosenAlleEIS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.30011-2015-01-31T000000.html"
              test="@nullFlavor or (@code='PFDIAG' and @codeSystem='1.2.40.0.34.5.40')">(PflegeBetreuungsdiagnosenAlleEIS): Der Elementinhalt MUSS einer von 'code 'PFDIAG' codeSystem '1.2.40.0.34.5.40'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.30011
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:title[not(@nullFlavor)]
Item: (PflegeBetreuungsdiagnosenAlleEIS)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:title[not(@nullFlavor)]"
         id="d79695e64-false-d79726e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.30011-2015-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(PflegeBetreuungsdiagnosenAlleEIS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.30011-2015-01-31T000000.html"
              test="text()='Pflegediagnosen'">(PflegeBetreuungsdiagnosenAlleEIS): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Pflegediagnosen'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.30011
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:text[not(@nullFlavor)]
Item: (PflegeBetreuungsdiagnosenAlleEIS)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:text[not(@nullFlavor)]"
         id="d79695e70-false-d79740e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.30011-2015-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(PflegeBetreuungsdiagnosenAlleEIS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.3.2.1
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.3.2.1']]/hl7:entry
Item: (PflegeBetreuungsdiagnosenEnhanced)
-->
</pattern>
